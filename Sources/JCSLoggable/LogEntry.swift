//
//  LogEntry.swift
//  Loggable
//
//  Created by Josh Wisenbaker on 10/14/18.
//

/// The `LogEntry` struct provides normalized log entries for the `LocalLog` storage. Do not access directly.
public struct LogEntry {

    /// The log level expressed as a member of the `LogLevel` enum.
    let logLevel: LogLevel

    /// The message that was logged as a `String`.
    let logMessage: String
}
