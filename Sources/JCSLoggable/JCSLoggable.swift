//
//  JCSLoggable.swift
//
//  Created by Josh Wisenbaker 6/9/2018.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//

import Cocoa
import os.log

/// The values in `logLevel` mirror the `OSLogType` vaules. We need this to work around `OSLogType`
/// only being avaliable on 10.12+
///
/// - `default`: Default-level messages are initially stored in memory buffers. Without a configuration change,
/// they are compressed and moved to the data store as memory buffers fill. They remain there until a storage quota is
/// exceeded,
/// at which point, the oldest messages are purged. Use this level to capture information about things that might result
/// a failure.
///
/// - info: Info-level messages are initially stored in memory buffers. Without a configuration change,
/// they are not moved to the data store and are purged as memory buffers fill. They are, however, captured
/// in the data store when faults and, optionally, errors occur. When info-level messages are added to
/// the data store, they remain there until a storage quota is exceeded, at which point, the oldest messages are purged.
/// Use this level to capture information that may be helpful, but isn’t essential, for troubleshooting errors.
///
/// - debug: Debug-level messages are only captured in memory when debug logging is enabled through a
/// configuration change. They’re purged in accordance with the configuration’s persistence setting
/// Messages logged at this level contain information that may be useful during development or while
/// troubleshooting a specific problem. Debug logging is intended for use in a development environment and not in
/// shipping software.
///
/// - error: Error-level messages are always saved in the data store. They remain there until a storage
/// quota is exceeded, at which point, the oldest messages are purged. Error-level messages are intended
/// for reporting process-level errors. If an activity object exists, logging at this level captures information
/// for the entire process chain.
///
/// - fault: Fault-level messages are always saved in the data store. They remain there until a
/// storage quota is exceeded, at which point, the oldest messages are purged. Fault-level messages
/// are intended for capturing system-level or multi-process errors only. If an activity object exists,
/// logging at this level captures information for the entire process chain.
public enum LogLevel {
    case `default`
    case info
    case debug
    case error
    case fault
}

/// The JCSLoggable protocol provides an easy way to log messages across platform versions.
/// On macOS 10.12 and higher, os_log is used. Earlier versions will use NSLog.
public protocol JCSLoggable {

    /// A `String` that defines the subsystem for sorting logs. Commonly set to the Bundle ID of the app.
    ///
    /// On macOS 10.12+ this is added as the OSLog category.
    var logSubsystem: String { get }

    /// A `String` that defines the category for sorting logs.
    ///
    /// On macOS 10.12+ this is added as the OSLog category.
    var logCategory: String { get }
}

public extension JCSLoggable {

    /// A `OSLog` that is auto populated with the main bundle identifier as the subsystem and
    /// the `logCategory` property as the category.
    ///
    /// Returns `nil` on versions of macOS earlier than 10.12
    var log: OSLog? {
        if #available(OSX 10.12, *) {
            return OSLog(subsystem: logSubsystem, category: logCategory)
        } else {
            return nil
        }
    }

    /// Logs a message to the `default` level.
    ///
    /// - Parameter message: A `String` containing the message to be logged.
    func logDefault(_ message: String) {
        logIt(message)
        LocalLog.shared.append(message, level: .default)
    }

    /// Logs a message to the `info` level.
    ///
    /// - Parameter message: A `String` containing the message to be logged.
    func logInfo(_ message: String) {
        logIt(message, level: .info)
        LocalLog.shared.append(message, level: .info)
    }

    /// Logs a message to the `debug` level.
    ///
    /// - Parameter message: A `String` containing the message to be logged.
    func logDebug(_ message: String) {
        logIt(message, level: .debug)
        LocalLog.shared.append(message, level: .debug)
    }

    /// Logs a message to the `error` level.
    ///
    /// - Parameter message: A `String` containing the message to be logged.
    func logError(_ message: String) {
        logIt(message, level: .error)
        LocalLog.shared.append(message, level: .error)
    }

    /// Logs a message to the `fault` level.
    ///
    /// - Parameter message: A `String` containing the message to be logged.
    func logFault(_ message: String) {
        logIt(message, level: .fault)
        LocalLog.shared.append(message, level: .fault)
    }

    /// Logs a message using the appropriate logging system.
    ///
    /// - Parameters:
    ///   - message: A `String` containing the message to be logged/
    ///   - level: The value from `logLevel` to represent the severity of the log. macOS 10.12+ will
    /// use the `OSLogType` that matches. Earlier versions of macOS will apply a hashtag to the NSLog statement.
    /// Defaults to the `default` log level if no level is entered.
    func logIt(_ message: String, level: LogLevel = .default) {
        if #available(OSX 10.12, *) {
            modernLog(level, message)
        } else {
            legacyLog(level, message)
        }
    }

    @available(OSX 10.12, *)
    private func modernLog(_ level: LogLevel, _ message: String) {
        switch level {
        case .default:
            os_log("%{public}@", log: log!, message)
        case .info:
            os_log("%{public}@", log: log!, type: .info, message)
        case .debug:
            os_log("%{public}@", log: log!, type: .debug, message)
        case .error:
            os_log("%{public}@", log: log!, type: .error, message)
        case .fault:
            os_log("%{public}@", log: log!, type: .fault, message)
        }
    }

    private func legacyLog(_ level: LogLevel, _ message: String) {
        switch level {
        case .default:
            NSLog("%@: #default %@", message, logCategory)
        case .info:
            NSLog("%@: #info %@", message, logCategory)
        case .debug:
            NSLog("%@: #debug %@", message, logCategory)
        case .error:
            NSLog("%@: #error %@", message, logCategory)
        case .fault:
            NSLog("%@: #fault %@", message, logCategory)
        }
    }
}
