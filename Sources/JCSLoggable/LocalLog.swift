//
//  LocalLog.swift
//  NoMADLoginOkta
//
//  Created by Josh Wisenbaker on 10/12/18.
//  Copyright © 2018 Orchard & Grove. All rights reserved.
//

/// `LocalLog` exists to store logged messages in memory if desired. Log messages are always sent to the system logger.
///
/// When using `os_log`, or even `NSLog`, messages that are logged are not easily avaliable to be used by an app or
/// library. The `Loggable` protocol includes a shared `LocalLog` that can provide easy access to log messages in
/// code adopting the protocol. The primary use of this is to display log messages in your project UI.
///
/// In order to enable the collection of logs you must set the `isEnabled` value of the `LocalLog.sharedInstance`
/// singleton to `true`. In this state all messages sent via the `Loggable` protocol will be sent to the system
/// logger and recorded in the `LocalLog`. The `localLog` property provides easy access to the messages as a newline
/// delimited `String`.
public final class LocalLog {

    /// A shared instance of the `LocalLog` type.
    public static let shared = LocalLog()

    /// A `String` that contains the logged messages if `isEnabled` is `true`. One line per-entry that is set with the
    /// `Loggable` protocol on macOS regardless of OS version.
    ///
    /// `localLog` will be empty if `isEnabled` is set to `false`.
   public var localLog: String

    /// An `Array` of `LogEntry` structs that represent log messages and cooresponding levels that are logged with
    /// `Loggable` when `isEnabled` is set to `true`.
    ///
    /// `logStorage` will be empty if `isEnabled` is set to `false`.
    var logStorage = [LogEntry]() {
        didSet {
            guard let message = logStorage.last?.logMessage else { return }
            localLog += "\(message)\n"
        }
    }

    /// A `Bool` that controls if a in-memory `LocalLog` is kept for each message logged to the system with `Loggable`.
    /// The default value is `false`.
    ///
    /// If `LocalLog` is in use and the value is set to `false` all `LocalLog` entries are removed. This does not effect
    /// the messages sent to the system logger by `Loggable`, only the duplicate copies are removed.
   public var isEnabled: Bool {
        willSet {
            if newValue == false {
                purge()
            }
        }
    }

    /// fileprivate initializer to ensure only the singleton exists.
    private init() {
        isEnabled = false
        localLog = ""
    }

    /// Adds a `LogEntry` to `logStorage`. Do not call directly.
    ///
    /// - Parameters:
    ///   - message: A `String` of the message to be logged.
    ///   - level: One of the `LoggLevel` enum values.
    public func append(_ message: String, level: LogLevel) {
        if isEnabled == true {
            let entry = LogEntry(logLevel: level, logMessage: message)
            logStorage.append(entry)
        }
    }

    /// The method will remove all messages from the `LocalLog`. Purging messages does not disable the capture of
    /// log messages.
    public func purge() {
        localLog = ""
        logStorage.removeAll()
    }
}
