[![Build status](https://dev.azure.com/OrchardAndGroveOSS/JCSLoggable/_apis/build/status/JCSLoggable-Xcode-CI)](https://dev.azure.com/OrchardAndGroveOSS/JCSLoggable/_build/latest?definitionId=1) [![codebeat badge](https://codebeat.co/badges/8cbfaa85-fa6a-42b9-baa3-81734c618269)](https://codebeat.co/projects/gitlab-com-orchardandgrove-oss-jcsloggable-master) [![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

# JCSLoggable

System logging tool for Apple platforms using Swift.

If you are 10.12+ it will use `os_log`, if you are older than that it will us `NSLog`. You don't need to worry about what version of the system logger that your OS has, just adopt the protocol and go!

## Adopting JCSLoggable
You can build the Framework using SPM. You can then either drop the built product into an Xcode project, or use SPM to encorporate it.

Then you just need to `import JCSLoggable` and then declare that your class adopts the protocol. This will prompt you to impliment two stubs:

`logSubsystem` is a `String` that defines the subsystem for sorting logs. Commonly this is set to the Bundle ID of the app and you can set it up as a global to make things simpler. At first this value would default to the BundleID of the app that was logging the entry, but for things like AuthorizationPlugins that gets messy as you aren't actually a running app. 

`logCategory` is a `String` that defines the category for sorting logs. This is normally set to an identifier that you will recognize like "UILogs" or whatever.

With those set you can log a message and the framework will route it and log without you needing to worry about the finicky `os_log` syntax.

## Basic Logging
In order to log a message there are a number of functions to call. They all start with "log", end with the log level that you want to apply, and take a `String` as the message type.
### Examples
```
logDefault("This is a default message")
logInfo("This is an info level log message")
logDebug("This is a debug level log message")
```

If you are running on macOS 10.12 or higher, JCSLoggable will use `os_log`.
If you are running on an older version of macOS, JCSLoggable will use `NSLog` and apply the log level as a hashtag in the message as recommended by Apple.

## Advanced Usage
One of the advanced features of Apple's Unified Logging is that everything is done in memory buffers and then lazily flushed to disk in a database according to the system log retention settings. This is awesome for performance, but can make it hard to actually see in your own app what you are logging. In order to make this easier, JCSLoggable impliments another in-memory log storage system that you can retrieve the contents of as a `String`. This feature is currently called `LocalLog`.

By default the LocalLog storage is disabled. It's simple to make work though by toggling a `Bool` on the shared Local Log object thusly: `LocalLog.shared.isEnabled = true`. Now each time you log a message it is also stored in an `Array` of `LogEntry` structs. You can easily retrieve the current logged messages as a string by accessing the `localLog` property on the shared instance. Each log entry will be seperated by a newline.

The goal of this feature is to provide an easy way to access your logged messages to display them in your UI.

### Example
```
// Enable locallog storage
LocalLog.shared.isEnabled = true

// Log a message
logDefault("This is a default message")

// Print the local log string
print(LocalLog.shared.localLog)
```

If you set `isEnabled` to `false` the shared instance will not only stop recording new messages, but it will purge the old ones as well. You can also interactivly empty the Local Log storage by calling `LocalLog.shared.purge()`. This will empty the JCSLoggable protocol log storage but will not stop the recording of new messages.
Importantly, none of these options have an effect on the system logger. It will always log your messages as expected.

## Todo
Future things in my head...
- Rename the `localLog` string property. Currently it feels a bit clunky.
- Add KVO support to the `localLog` property so that you can just observe it and update your UI as needed.
- Add the log message level to the string output.
- Allow access to the `logStorage` array of `LogEntry` structs. Maybe.