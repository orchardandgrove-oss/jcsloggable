import XCTest
import os.log
import Cocoa
@testable import JCSLoggable

final class JCSLoggableTests: XCTestCase {
    class SutClass: JCSLoggable {
        var logSubsystem = "loggable.tests"
        let logCategory = "testing"
    }

    let sut = SutClass()

    override func setUp() {
        LocalLog.shared.isEnabled = true
    }

    override func tearDown() {
        LocalLog.shared.isEnabled = false
    }

    func testHasLog() {
        if #available(OSX 10.12, *) {
            XCTAssertNotNil(sut.log, "Could not find the log for the class")
        } else {
            XCTAssert(true, "Running lower than macOS 10.12. No OSLog expected")
        }
    }

    func testLogDefault() {
        sut.logDefault("This is a default log message")
        XCTAssert(LocalLog.shared.localLog == "This is a default log message\n",
                  "Local log string wasn't set properly.")
    }

    func testLogInfo() {
        sut.logInfo("This is a info log message")
        XCTAssert(LocalLog.shared.localLog == "This is a info log message\n",
                  "Local log string wasn't set properly.")
    }

    func testLogDebug() {
        sut.logDebug("This is a debug log message")
    }

    func testLogError() {
        sut.logError("This is a error log message")
    }

    func testLogFault() {
        sut.logFault("This is a fault log message")
    }

    func testLogPerformance() {
        self.measure {
            sut.logDefault("This is a default log message")
            sut.logInfo("This is a info log message")
            sut.logDebug("This is a debug log message")
            sut.logError("This is a error log message")
            sut.logFault("This is a fault log message")
        }
    }

}
