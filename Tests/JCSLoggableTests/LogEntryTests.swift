//
//  LogEntryTests.swift
//  LoggableTests
//
//  Created by Josh Wisenbaker on 10/14/18.
//

import XCTest
@testable import JCSLoggable

class LogEntryTests: XCTestCase {
    let sut = LogEntry(logLevel: .default, logMessage: "This is a message")

    func testBasicEntry() {
        XCTAssert(sut.logLevel == .default, "Log level wasn't set properly.")
        XCTAssert(sut.logMessage == "This is a message", "Log message wasn't set properly.")
    }
}
