//
//  LocalLogTests.swift
//  LoggableTests
//
//  Created by Josh Wisenbaker on 10/14/18.
//

import XCTest
@testable import JCSLoggable

class LocalLogTests: XCTestCase {

    let sut = LocalLog.shared

    func testLocalLogAppend() {
        sut.isEnabled = true
        sut.append("testing", level: .default)
        XCTAssertEqual(sut.localLog, "testing\n", "Local log was not set properly")
        sut.append("testing", level: .info)
        XCTAssertEqual(sut.localLog, "testing\ntesting\n", "Local log was not set properly")
    }

    func testLocalLogPerformance() {
        self.measure {
            sut.isEnabled = true
            sut.append("testing", level: .default)
            sut.append("testing", level: .info)
        }
    }

    func testPurgeLocalLog() {
        sut.isEnabled = true
        sut.append("testing", level: .debug)
        sut.purge()
        XCTAssert(sut.localLog.isEmpty, "Log was not purged properly.")
    }

    func testSetInactivePurge() {
        sut.isEnabled = true
        sut.append("testing", level: .fault)
        XCTAssertEqual(sut.localLog, "testing\n", "Local log was not set properly")
        sut.isEnabled = false
        XCTAssert(sut.localLog.isEmpty, "Log was not purged properly.")
    }

    func testInactiveDoesNotAppend() {
        sut.isEnabled = false
        sut.append("should not accept", level: .error)
        XCTAssert(sut.localLog.isEmpty, "Log was not purged properly.")
    }
}
