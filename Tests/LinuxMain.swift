import XCTest

import JCSLoggableTests

var tests = [XCTestCaseEntry]()
tests += JCSLoggableTests.allTests()
XCTMain(tests)